package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.MainActivity;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ShoppingItemDB extends  MainActivity{

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        //muestra el repositorio de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //éste objeto crea un nuevo mapa de valores en
        // donde los nombres de cada columna son las llaves
        // y con la instancia valores se invoca al método put
        ContentValues valores = new ContentValues();
        valores.put(ShoppingElementEntry.COLUMN_NAME_TITLE,productName);
        String select = ShoppingElementEntry.COLUMN_NAME_TITLE+ " = ?";
        //esta linea inserta una nueva fila devolviendo el valor
        // de la clave principal de la fila creada
        db.insert(ShoppingElementEntry.TABLE_NAME, select, valores);


    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        //muestra el repositorio de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //se utiliza el metodo delete con el nombre de la tabla y los parametros WHERE en
        // nulos para que borre la lista y no se repitan los elementos
        db.delete(ShoppingElementEntry.TABLE_NAME,null,null);
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        //muestra el repositorio de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //para el método update se requieren 4 parametros que son:
        // el nombre de la tabla, la instancia de contentValues,
        // luego se  obtiene el  valor antiguo con el método put,
        //luego con select se selecciona donde se ubica el nuevo valor, y
        // con selectArgs se obtiene el nuevo valor con el getId
        //el nuevo valor y por ultimo se utiliza el método
        ContentValues valoresNuevos = new ContentValues();
        valoresNuevos.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName());
        String select = ShoppingElementEntry._ID+ " = ?";
        String [] selectArgs = {String.valueOf(shoppingItem.getId())};
        db.update(ShoppingElementEntry.TABLE_NAME,valoresNuevos, select,selectArgs);

    }
    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        //muestra el repositorio de datos en modo escritura
        SQLiteDatabase deleteItem = dbHelper.getWritableDatabase();
        String select = ShoppingElementEntry.COLUMN_NAME_TITLE+ " = ?";
        String [] selectArgs = {shoppingItem.getName()};
        //metodo delete con los parametros select y selectArgs
        deleteItem.delete(ShoppingElementEntry.TABLE_NAME, select,selectArgs);
    }
}
